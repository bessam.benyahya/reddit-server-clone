#!/bin/bash

echo version?
read VERSION

docker build -t binovlab/lireddit:$VERSION .
docker push binovlab/lireddit:$VERSION

ssh root@167.172.248.5 "docker pull binovlab/lireddit:$VERSION && docker tag binovlab/lireddit:$VERSION dokku/api:$VERSION && dokku deploy api $VERSION"